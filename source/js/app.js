import noUiSlider from 'nodeModules/nouislider/distribute/nouislider.js';
import wNumb from 'nodeModules/wnumb/wNumb.js';


(function () {

  'use strict';

  // fancybox video
  $('.js-fancy-video').fancybox({
    maxWidth: 800,
    maxHeight: 600,
    fitToView: false,
    width: '70%',
    height: '70%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
  });

  // slider
  let slider1 = document.getElementById('slids__rule-up-1');
  let slider2 = document.getElementById('slids__rule-up-2');
  let slideOpt = {
    start: 0,
    animate: false,
    step: 1000,
    range: {
      min: 0,
      '25%': [10000, 1000],
      '48%': [20000, 1000],
      '65%': [40000, 1000],
      max: 100000
    }
  };
  noUiSlider.create(slider1, slideOpt);
  noUiSlider.create(slider2, slideOpt);

  let valueBigS = 0;
  let valueSmallS = 0;
  let moneyFormat = wNumb({
    prefix: '',
    decimals: 0,
    thousand: ' '
  });
  let resPay1 = $('.js-resPay1');
  let resPay2 = $('.js-resPay2');
  slider1.noUiSlider.on('update', function (values, handle) {
    if (handle == 0) {
      let ll = (values * 1) / 29;
      ll = ll - (ll % 1);
      if ((ll * 12) >= 15000) {
        ll = 1250;
      }
      valueBigS = moneyFormat.to(ll * 12);
      resPay1.text(moneyFormat.to(values * 1));
      result('big');
    }
  });
  slider2.noUiSlider.on('update', function (values, handle) {
    if (handle == 0) {
      let ll = (values * 1) / 80;
      ll = ll - (ll % 1);
      valueSmallS = moneyFormat.to(ll * 12);
      resPay2.text(moneyFormat.to(values * 1));
      result('small');
    }
  });

  let kk = 0;
  let bb = 0;
  let ss = 0;

  function result(x) {
    bb = moneyFormat.from(valueBigS);
    ss = moneyFormat.from(valueSmallS);
    kk = bb + ss;
    if (kk > 15000) {
      if (x === 'small') {
        ss = 15000 - bb;
        $('.js-res2').text(moneyFormat.to(ss));
        $('.js-res1 ').text(valueBigS);
      }
      if (x === 'big') {
        bb = 15000 - ss;
        $('.js-res1').text(moneyFormat.to(bb));
        $('.js-res2').text(valueSmallS);
      }
      $('.resPayAll').text(moneyFormat.to(15000));
    } else {
      $('.js-res1 ').text(valueBigS);
      $('.js-res2').text(valueSmallS);
      $('.resPayAll').text(moneyFormat.to(kk));
    }
  }

  // dropdown
  $('.select__dropdown').click(function () {
    $(this).toggleClass('state--active');
  });

  $('.select__dropdown-item').click(function () {
    let newItem = $(this).text();
    $(this).addClass('state--active').siblings().removeClass('state--active');
    $('.select__dropdown-active').text(newItem);
    $('.select-bank-text__item')
      .eq($(this).index())
      .addClass('state--active')
      .siblings().removeClass('state--active');
  });

  // .tabs-button
  $('.tabs-button').click(function () {
    $(this).addClass('state--active').siblings().removeClass('state--active');
    $('.tabs__item').eq($(this).index()).addClass('state--active')
      .siblings().removeClass('state--active');
    $('body').scrollTo($('.tabs'), 500, {axis: 'y'});
  });

  // .tips
  $('.tips-active').mouseenter(function () {
    $(this).parents('.tips').css({'z-index': 1000});
    $(this).next().show();
  }).mouseout(function () {
    $(this).next().hide();
    $(this).parents('.tips').css({'z-index': 1});
  });

})();
