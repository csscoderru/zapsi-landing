<!DOCTYPE html>
<html lang="{{ lang }}">

<head>
  {% if metaTitle %}<title>{{ metaTitle }}</title> {% else %} <title>Title {{ project }}</title> {% endif %}
  {% if metaDescription %}<meta name="description" content="{{ metaDescription }}"> {% endif %}
  {% if metaKeywords %}<meta name="keywords" content="{{ metaKeywords }}"> {% endif %}
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  {% if oggTitle %}<meta property="og:title" content="{{ oggTitle }}">{% endif %}
  {% if oggDescription %}<meta property="og:description" content="{{ oggDescription }}"> {% endif %}
  {% if oggType %}<meta property="og:type" content="{{ oggType }}"> {% else %} <meta property="og:type" content="website"> {% endif %}
  {% if ogUrl %}<meta property="og:url" content="{{ ogUrl }}"> {% endif %}
  {% if ogImage %}<meta property="og:image" content="{{ ogImage }}"> {% endif %}
  {% if ogSiteName %}<meta content="{{ ogSiteName }}" property="og:site_name" /> {% endif %}
  {% if twitterUserpic %}<meta content="{{ twitterUserpic }}" name="twitter:image:src" /> {% endif %}
  {% if twitterSite %}<meta content="{{ twitterSite }}" name="twitter:site" />{% endif %}
  {% if twitterCard %}<meta content="{{ twitterCard }}" name="twitter:card" />{% endif %}
  {% if twitterTitle %}<meta content="{{ twitterTitle }}" name="twitter:title" />{% endif %}
  {% if twitterDecription %}<meta content="{{ twitterDecription }}" name="twitter:description" />{% endif %}
  <link rel="apple-touch-icon" sizes="57x57" href="{{ path }}img/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="{{ path }}img/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="{{ path }}img/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ path }}img/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="{{ path }}img/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ path }}img/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="{{ path }}img/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ path }}img/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ path }}img/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ path }}img/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ path }}img/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ path }}img/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ path }}img/favicon/favicon-16x16.png">
  <link rel="manifest" href="{{ path }}img/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{ path }}img/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, shrink-to-fit=no">
  <link rel="stylesheet" href="{{ path }}css/main.css" media="all">
  <link href="https://fonts.googleapis.com/css?family=Cuprum:400,700|Roboto:400,700" rel="stylesheet">
</head>

<body class="l-body {{ layout_class }}">
<!-- page start -->
<div class="page">
  {% block content %}{% endblock %}
</div>
<!-- page finish -->
<script src="{{ path }}js/common.js"></script>
<script src="{{ path }}js/app.js"></script>
</body>

</html>
